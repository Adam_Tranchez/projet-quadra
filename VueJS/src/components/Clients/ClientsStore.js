import Vuex from 'vuex'

const state = {
    clients: [{
        name: 'Premier client',
        completed: true
    }]
}

const mutations = {
    ADD_CLIENT: (state, name) => {
        state.clients.push({
            name: name,
            completed: false
        })
    }
}

let Store = new Vuex.Store({
    state: state,
    mutations: mutations,
    getters: {},
    actions: {}
})

global.store = store

export default store 