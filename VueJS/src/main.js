import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import 'bulma/css/bulma.css'
import {ClientTable, Event} from 'vue-tables-2';
import Vuex from 'vuex'
import VueRouter from 'vue-router'

Vue.use(Vuex)

new Vue({
  el: '#app',
  VueRouter,
  render: h => h(App)
})
