import Vue from 'vue'
import Router from 'vue-router'
import Navbar from '@/components/Navbar/Navbar'
import Clients from '@/components/Clients/Clients'
import InfoClient from '@/components/InfoClient/InfoClient'
import InscrireClient from '@/components/InscrireClient/InscrireClient'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'navbar',
            component: Navbar
        },
        {
            path: '/clients',
            name: 'clients',
            component: Clients
        },
        {
            path: '/info-client',
            name: 'info-client',
            component: InfoClient
        },
        {
            path: '/inscrire-client',
            name: 'inscrire-client',
            component: InscrireClient
        }
    ]
})